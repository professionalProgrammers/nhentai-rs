# nhentai-rs
[![Build Status](https://travis-ci.com/professionalProgrammers/nhentai-rs.svg?branch=master)](https://travis-ci.com/bitbucket/professionalProgrammers/nhentai-rs)
[![](https://tokei.rs/b1/bitbucket.org/professionalProgrammers/nhentai-rs)](https://bitbucket.org/professionalProgrammers/nhentai-rs)

A nhentai lib for Rust that uses the api and performs no scraping. There is also an `nhentai-dl` cli app that demonstrates the intended usage of this library.

## Example
```rust
#[tokio::main]
async fn main() {
    let client = nhentai::Client::new();

    let comic = client.get_random().await.expect("Random Comic");

    println!("Got random comic: {:#?}", comic);
}
```

## Features
`cli`: Off by default, includes cli dependencies.

## Testing
Use `cargo test` to test. There are tests for using this lib with a socks proxy, but they are off by default. 
Set the `PROXY` environment variable to a valid socks proxy and run `cargo test -- --ignored` to run these tests.
Socks is not tested on CI, so it may be non-functional. Issues and PRs are welcome.

## Contributing
Issues and PRs are welcome. 

## License
This library is dual-licensed under [Apache](./LICENSE-APACHE) and [MIT](./LICENSE-MIT).
