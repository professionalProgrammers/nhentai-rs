use anyhow::Context;
use std::{
    convert::TryInto,
    num::NonZeroU64,
    path::PathBuf,
};
use tokio::{
    fs::File,
    io::AsyncWriteExt,
};
use url::Url;

#[derive(argh::FromArgs)]
#[argh(description = "A tool to download nhentai comics")]
pub struct Options {
    #[argh(positional, description = "the nhentai comic id")]
    pub id: NonZeroU64,

    #[argh(
        option,
        short = 'o',
        default = "PathBuf::from(\".\")",
        description = "the directory to output the comic"
    )]
    pub out_dir: PathBuf,
}

fn main() -> anyhow::Result<()> {
    let options: Options = argh::from_env();
    let tokio_rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()?;
    tokio_rt.block_on(async_main(options))?;
    Ok(())
}

async fn async_main(options: Options) -> anyhow::Result<()> {
    let client = nhentai::Client::new();

    let comic = client
        .get_comic(options.id)
        .await
        .context("failed to get comic data")?;

    let num_pages_usize = comic
        .num_pages
        .try_into()
        .context("the number of comic book pages cannot fit in a usize")?;

    let base_path = options.out_dir.join(options.id.to_string());
    let current_path = base_path.clone();

    println!("Title: '{}'", comic.title.pretty);
    println!("Id: {}", comic.id);
    println!("# of Pages: {}", comic.num_pages);
    println!("Out Dir: {}", current_path.display());
    println!();

    tokio::fs::create_dir_all(&base_path)
        .await
        .context("failed to create out dir")?;

    println!("Downloading {} images...", num_pages_usize);
    let (tx, mut rx) = tokio::sync::mpsc::channel(num_pages_usize);
    for url in comic.iter_image_urls() {
        let tx = tx.clone();
        let client = client.clone();
        let base_path = base_path.clone();
        tokio::spawn(async move {
            let result = download_image(client, url, base_path).await;
            let _ = tx.send(result).await.is_ok();
        });
    }
    drop(tx);

    let mut total_success = 0;
    while let Some(result) = rx.recv().await {
        match result {
            Ok(filename) => {
                total_success += 1;
                eprintln!(
                    "Downloaded {}/{} (filename = \"{}\")",
                    total_success, num_pages_usize, filename
                );
            }
            Err(error) => {
                eprintln!("Failed to download image: {error:?}");
            }
        }
    }

    let metadata_path = current_path.join("meta.json");
    let mut meta = File::create(&metadata_path)
        .await
        .with_context(|| format!("failed to create file '{}'", current_path.display()))?;
    let data = serde_json::to_vec_pretty(&comic)?;
    meta.write_all(&data).await?;
    meta.flush().await?;
    meta.sync_all().await?;

    download_image(
        client.clone(),
        comic.cover_image_url(),
        current_path.clone(),
    )
    .await
    .context("failed to download cover image")?;
    download_image(
        client.clone(),
        comic.thumbnail_image_url(),
        current_path.clone(),
    )
    .await
    .context("failed to download thumbnail image")?;

    Ok(())
}

async fn download_image(
    client: nhentai::Client,
    url: Url,
    base_path: PathBuf,
) -> anyhow::Result<String> {
    let file_name = url
        .path_segments()
        .and_then(|iter| iter.last())
        .context("missing filename")?;
    let current_path = base_path.join(file_name);

    match tokio::fs::metadata(&current_path).await {
        Ok(_metadata) => {
            return Ok(file_name.to_string());
        }
        Err(e) if e.kind() == std::io::ErrorKind::NotFound => {}
        Err(e) => {
            return Err(e).context("failed to check path");
        }
    }

    nd_util::download_to_path(&client.client, url.as_str(), current_path).await?;

    Ok(file_name.to_string())
}
