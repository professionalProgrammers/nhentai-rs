use crate::{
    Comic,
    ComicIndex,
    Error,
    FIRST_COMIC_ID,
    USER_AGENT_STR,
};
use rand::Rng;
use reqwest::header::{
    HeaderMap,
    HeaderValue,
    USER_AGENT,
};
use reqwest_cookie_store::{
    CookieStore,
    CookieStoreMutex,
};
use std::{
    num::NonZeroU64,
    sync::{
        Arc,
        Mutex,
    },
};

/// The page of the first index
const FIRST_INDEX_PAGE: NonZeroU64 = NonZeroU64::MIN;

static ACCEPT_HEADER_VALUE: HeaderValue = HeaderValue::from_static("text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7");
static ACCEPT_LANGUAGE_VALUE: HeaderValue = HeaderValue::from_static("en-US,en;q=0.9");

/// NHentai client
#[derive(Clone)]
pub struct Client {
    /// The inner http client
    ///
    /// It probably shouln't be used directly.
    pub client: reqwest::Client,

    /// The cookie store.
    pub cookie_store: Arc<CookieStoreMutex>,

    /// The user agent
    pub user_agent: Arc<Mutex<Arc<str>>>,
}

impl Client {
    /// Make a new client.
    pub fn new() -> Self {
        let mut default_headers = HeaderMap::new();
        default_headers.insert(reqwest::header::ACCEPT, ACCEPT_HEADER_VALUE.clone());
        default_headers.insert(
            reqwest::header::ACCEPT_LANGUAGE,
            ACCEPT_LANGUAGE_VALUE.clone(),
        );

        let cookie_store = CookieStore::default();
        let cookie_store = Arc::new(CookieStoreMutex::new(cookie_store));
        let client = reqwest::ClientBuilder::new()
            .cookie_provider(cookie_store.clone())
            .default_headers(default_headers)
            .build()
            .expect("failed to build the nhentai client");

        Self::from_parts(client, cookie_store)
    }

    /// Make a new [`Client`] from a pre-existing client and a cookie store.
    pub fn from_parts(client: reqwest::Client, cookie_store: Arc<CookieStoreMutex>) -> Self {
        Client {
            client,
            cookie_store,
            user_agent: Arc::new(Mutex::new(USER_AGENT_STR.into())),
        }
    }

    /// Set the current user agent.
    ///
    /// # Returns
    /// Returns the old user agent.
    pub fn set_user_agent(&self, new_user_agent: &str) -> Arc<str> {
        let mut new_user_agent = Arc::from(new_user_agent);
        let mut old_user_agent = self
            .user_agent
            .lock()
            .unwrap_or_else(|error| error.into_inner());

        std::mem::swap(&mut *old_user_agent, &mut new_user_agent);

        new_user_agent
    }

    async fn get(&self, url: &str) -> Result<reqwest::Response, Error> {
        let user_agent = self
            .user_agent
            .lock()
            .unwrap_or_else(|error| error.into_inner())
            .clone();

        Ok(self
            .client
            .get(url)
            .header(USER_AGENT, &*user_agent)
            .send()
            .await?)
    }

    /// Get the comic with the specified id.
    pub async fn get_comic(&self, id: NonZeroU64) -> Result<Comic, Error> {
        let url = format!("https://nhentai.net/api/gallery/{id}");
        let response = self.get(url.as_str()).await?.error_for_status()?;
        let comic = response.json().await?;
        Ok(comic)
    }

    /// Get a page of the index of all comics.
    ///
    /// `page` starts at 1.
    pub async fn get_index(&self, page: NonZeroU64) -> Result<ComicIndex, Error> {
        let url = format!("https://nhentai.net/api/galleries/all?page={page}",);
        let response = self.get(&url).await?.error_for_status()?;
        let index = response.json().await?;
        Ok(index)
    }

    /// Get a random comic.
    ///
    /// This is mostly a crude helper function, and could possibly be more efficient.
    /// Look at the source to customize for your use-case.
    pub async fn get_random(&self) -> Result<Comic, Error> {
        let index = self.get_index(FIRST_INDEX_PAGE).await?;

        // TODO: Make a proper error?
        let total = NonZeroU64::new(index.num_pages * index.per_page).unwrap();
        let id = rand::thread_rng().gen_range(FIRST_COMIC_ID.get()..total.get());

        // We generate a non-zero number above,
        // so we can unwrap here.
        let id = NonZeroU64::new(id).unwrap();

        self.get_comic(id).await
    }
}

impl Default for Client {
    fn default() -> Self {
        Self::new()
    }
}
