pub mod comic;
pub mod comic_index;

pub use self::{
    comic::{
        Comic,
        Image,
        ImageKind,
        Images as ComicImages,
        TagKind,
    },
    comic_index::ComicIndex,
};
