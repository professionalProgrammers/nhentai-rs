/// Library error type
#[derive(thiserror::Error, Debug)]
pub enum Error {
    /// Reqwest error
    #[error("reqwest http error")]
    Reqwest(
        #[from]
        #[source]
        reqwest::Error,
    ),
}
