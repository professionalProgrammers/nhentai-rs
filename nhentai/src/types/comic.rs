use reqwest::Url;
use serde::{
    Deserialize,
    Deserializer,
};
use std::{
    collections::HashMap,
    convert::TryFrom,
    fmt::Display,
    num::NonZeroU64,
    str::FromStr,
};

/// A Comic
#[derive(Clone, Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct Comic {
    /// The unique id for this comic
    #[serde(deserialize_with = "deserialize_from_str_or_num")]
    pub id: NonZeroU64,

    /// The number of pages in this comic
    pub num_pages: u32,

    /// The name of the scanlator
    pub scanlator: Box<str>,

    /// Tags
    pub tags: Vec<Tag>,

    /// Number of favorites
    pub num_favorites: u64,

    /// Upload date
    #[serde(with = "time::serde::timestamp")]
    pub upload_date: time::OffsetDateTime,

    /// Media id
    #[serde(
        serialize_with = "serialize_to_str",
        deserialize_with = "deserialize_from_str"
    )]
    pub media_id: u64,

    /// Comic images
    pub images: Images,

    /// Comic Title
    pub title: Title,

    /// Unknown data
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

impl Comic {
    /// Iter over each page url
    pub fn iter_page_urls(&self) -> impl Iterator<Item = Url> + '_ {
        let id = self.id;
        self.images.pages.iter().enumerate().map(move |(i, _img)| {
            let image_num = i + 1;

            // This should always be a valid url
            let url_string = format!("https://nhentai.net/g/{id}/{image_num}/");
            Url::parse(&url_string).unwrap()
        })
    }

    /// Iter over image urls
    pub fn iter_image_urls(&self) -> impl Iterator<Item = Url> + '_ {
        self.iter_image_urls_with_base_url("https://i.nhentai.net/galleries")
    }

    /// Iter over image urls with the given base url.
    ///
    /// Useful for when using mirror sites.
    pub fn iter_image_urls_with_base_url<'a>(
        &'a self,
        base_url: &'a str,
    ) -> impl Iterator<Item = Url> + 'a {
        let media_id = self.media_id;
        self.images.pages.iter().enumerate().map(move |(i, img)| {
            let image_num = i + 1;
            let image_ext = img.kind.get_ext();

            // This should always be a valid url
            let url_string = format!("{base_url}/{media_id}/{image_num}.{image_ext}");
            Url::parse(&url_string).unwrap()
        })
    }

    /// Get the cover image url as a [`String`].
    pub fn cover_image_url_string(&self) -> String {
        self.cover_image_url_string_with_base_url("https://t.nhentai.net/galleries")
    }

    /// Get the cover image url as a [`String`].
    ///
    /// Useful for when using mirror sites.
    pub fn cover_image_url_string_with_base_url(&self, base_url: &str) -> String {
        let media_id = self.media_id;
        let image_ext = self.images.cover.kind.get_ext();
        format!("{base_url}/{media_id}/cover.{image_ext}")
    }

    /// Get the cover image url as a [`Url`].
    pub fn cover_image_url(&self) -> Url {
        // This should always produce a valid url
        Url::parse(&self.cover_image_url_string()).unwrap()
    }

    /// Get the cover image url as a [`Url`].
    ///
    /// Useful for when using mirror sites.
    ///
    /// # Panics
    /// Panics if base_url is not a valid url.
    pub fn cover_image_url_with_base_url(&self, base_url: &str) -> Url {
        // This should always produce a valid url
        Url::parse(&self.cover_image_url_string_with_base_url(base_url)).unwrap()
    }

    /// Get the thumbnail image url as a [`String`].
    pub fn get_thumbnail_image_url_string(&self) -> String {
        self.get_thumbnail_image_url_string_with_base_url("https://t.nhentai.net/galleries")
    }

    /// Get the thumbnail image url as a [`String`].
    ///
    /// Useful for when using mirror sites.
    pub fn get_thumbnail_image_url_string_with_base_url(&self, base_url: &str) -> String {
        let media_id = self.media_id;
        let image_ext = self.images.thumbnail.kind.get_ext();
        format!("{base_url}/{media_id}/thumb.{image_ext}")
    }

    /// Get the thumbnail image url as a [`Url`].
    pub fn thumbnail_image_url(&self) -> Url {
        // This should always produce a valid url
        Url::parse(&self.get_thumbnail_image_url_string()).unwrap()
    }

    /// Get the thumbnail image url as a [`Url`].
    ///
    /// Useful for when using mirror sites.
    ///
    /// # Panics
    /// Panics if base_url is not a valid url.
    pub fn thumbnail_image_url_with_base_url(&self, base_url: &str) -> Url {
        // This should always produce a valid url
        Url::parse(&self.get_thumbnail_image_url_string_with_base_url(base_url)).unwrap()
    }
}

/// Comic Tags
#[derive(Clone, Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct Tag {
    /// Number of items associated with tag
    pub count: u64,

    /// Tag id
    pub id: u64,

    /// Tag name
    pub name: Box<str>,

    /// Tag kind
    #[serde(rename = "type")]
    pub kind: TagKind,

    /// Url to tag
    pub url: Box<str>,

    /// Unknown info
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

/// The kind of tag
#[derive(Clone, Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub enum TagKind {
    #[serde(rename = "language")]
    Language,

    #[serde(rename = "parody")]
    Parody,

    #[serde(rename = "character")]
    Character,

    #[serde(rename = "tag")]
    Tag,

    #[serde(rename = "group")]
    Group,

    #[serde(rename = "artist")]
    Artist,

    #[serde(rename = "category")]
    Category,
}

/// Comic Images
#[derive(Clone, Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct Images {
    /// Cover image
    pub cover: Image,

    /// Pages
    pub pages: Vec<Image>,

    /// Thumbnail image
    pub thumbnail: Image,

    /// Unknown Info
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

/// Image data
#[derive(Clone, Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct Image {
    /// Image height
    #[serde(rename = "h")]
    pub height: u32,

    /// Image width
    #[serde(rename = "w")]
    pub width: u32,

    /// Image kind
    #[serde(rename = "t")]
    pub kind: ImageKind,

    /// Unknown data
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

/// The image kind
#[derive(Copy, Clone, Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub enum ImageKind {
    #[serde(rename = "j")]
    Jpeg,

    #[serde(rename = "p")]
    Png,

    #[serde(rename = "g")]
    Gif,

    #[serde(rename = "w")]
    Webp,
}

impl ImageKind {
    /// Get the extension as a string.
    ///
    /// Does not have a leading "."
    pub fn get_ext(self) -> &'static str {
        match self {
            Self::Jpeg => "jpg",
            Self::Png => "png",
            Self::Gif => "gif",
            Self::Webp => "webp",
        }
    }
}

/// A comic title
#[derive(Clone, Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub struct Title {
    /// English title
    pub english: Box<str>,

    /// Japanese title
    pub japanese: Option<Box<str>>,

    /// Pretty title
    pub pretty: Box<str>,

    /// Unknown titles
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

fn serialize_to_str<T, S>(t: &T, serializer: S) -> Result<S::Ok, S::Error>
where
    T: ToString,
    S: serde::Serializer,
{
    serializer.serialize_str(&t.to_string())
}

fn deserialize_from_str<'de, T, D>(deserializer: D) -> Result<T, D::Error>
where
    T: FromStr,
    T::Err: Display,
    D: Deserializer<'de>,
{
    let s = Deserialize::deserialize(deserializer)?;
    T::from_str(s).map_err(serde::de::Error::custom)
}

fn deserialize_from_str_or_num<'de, T, D>(deserializer: D) -> Result<T, D::Error>
where
    T: FromStr,
    T: TryFrom<u64>,
    <T as TryFrom<u64>>::Error: std::fmt::Display,
    <T as FromStr>::Err: Display,
    T: Deserialize<'de>,
    D: Deserializer<'de>,
{
    match serde_json::Value::deserialize(deserializer)? {
        serde_json::Value::String(s) => T::from_str(&s).map_err(serde::de::Error::custom),
        serde_json::Value::Number(n) => {
            if let Some(n) = n.as_u64() {
                T::try_from(n).map_err(serde::de::Error::custom)
            } else {
                Err(serde::de::Error::custom("No type match"))
            }
        }
        _ => Err(serde::de::Error::custom("No type match")),
    }
}

#[cfg(test)]
mod test {
    use super::*;

    const COMIC: &str = include_str!("../../test_data/comic.json");

    #[test]
    fn parse_comic() {
        let comic: Comic = serde_json::from_str(COMIC).unwrap();
        assert_eq!(comic.id.get(), 1);
    }
}
