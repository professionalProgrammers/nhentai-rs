use crate::Comic;
use std::collections::HashMap;

/// Index of all comics
#[derive(Debug, Clone, PartialEq, Eq, serde::Deserialize, serde::Serialize)]
pub struct ComicIndex {
    /// The total number of pages in the index
    pub num_pages: u64,

    /// The number of entries per page
    pub per_page: u64,

    /// Comics
    pub result: Vec<Comic>,

    /// Unknown data
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

#[cfg(test)]
mod test {
    use super::*;

    const COMIC_INDEX: &str = include_str!("../../test_data/comic_index.json");
    const COMIC_INDEX_GIF: &str = include_str!("../../test_data/comic_index_gif.json");

    #[test]
    fn parse_comic_index() {
        let comic_index: ComicIndex = serde_json::from_str(COMIC_INDEX).unwrap();
        assert_eq!(comic_index.num_pages, 13192);
        assert_eq!(comic_index.per_page, 25);
    }

    #[test]
    fn parse_comic_index_gif() {
        let comic_index: ComicIndex = serde_json::from_str(COMIC_INDEX_GIF).unwrap();
        assert_eq!(comic_index.num_pages, 13366);
        assert_eq!(comic_index.per_page, 25);
    }
}
