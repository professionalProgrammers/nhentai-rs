mod client;
mod error;
pub mod types;

pub use crate::{
    client::Client,
    error::Error,
    types::{
        Comic,
        ComicImages,
        ComicIndex,
        Image,
        ImageKind,
        TagKind,
    },
};
pub use cookie_store::CookieStore;
pub use reqwest::{
    ClientBuilder as ReqwestClientBuilder,
    Url,
};
pub use reqwest_cookie_store::CookieStoreMutex;
use std::num::NonZeroU64;

/// The User Agent String.
///
/// I would like to be nice and not lie.
/// However nhentai is making this hard on me so I don't even care anymore.
pub const USER_AGENT_STR: &str = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36";

/// The ID of the first comic
pub const FIRST_COMIC_ID: NonZeroU64 = NonZeroU64::MIN;

#[cfg(test)]
mod test {
    use super::*;
    use cookie_store::RawCookie;
    use std::sync::OnceLock;

    /// Test Config
    #[derive(serde::Deserialize)]
    pub struct Config {
        /// The cloudflare bypass server to use to bypass cloudflare
        #[serde(rename = "cloudflare-bypass-server")]
        pub cloudflare_bypass_server: Url,
    }

    impl Config {
        /// Load the Config
        fn load() -> Self {
            let data = std::fs::read_to_string("test.toml").expect("failed to load test.toml");
            let data: Self = toml::from_str(&data).expect("failed to parse test.toml");

            data
        }

        /// Get the config
        pub fn get() -> &'static Self {
            static CONFIG: OnceLock<Config> = OnceLock::new();

            CONFIG.get_or_init(Self::load)
        }
    }

    #[derive(serde::Deserialize)]
    struct BypassData {
        user_agent: String,
        cf_clearance: String,
    }

    /// Get a client
    async fn get_client() -> Client {
        static CLIENT: tokio::sync::OnceCell<Client> = tokio::sync::OnceCell::const_new();

        CLIENT
            .get_or_init(|| async {
                let client = Client::new();
                let config = Config::get();

                let mut url = config.cloudflare_bypass_server.clone();
                url.set_path("/api/bypass");
                url.query_pairs_mut()
                    .append_pair("url", "https://nhentai.net");

                let bypass_data = client
                    .client
                    .post(url.as_str())
                    .send()
                    .await
                    .expect("failed to send bypass request")
                    .error_for_status()
                    .expect("invalid status in bypass response")
                    .json::<BypassData>()
                    .await
                    .expect("invalid bypass data json");

                client.set_user_agent(bypass_data.user_agent.as_str());

                {
                    let mut cookie_store = client.cookie_store.lock().unwrap();
                    let cookie = RawCookie::parse(&*bypass_data.cf_clearance)
                        .expect("failed to parse cf_clearance cookie");
                    let domain = cookie.domain().expect("cf_clearance cookie missing domain");
                    let url = format!("http://{domain}");
                    let url = Url::parse(&url).expect("failed parse domain");
                    cookie_store
                        .insert_raw(&cookie, &url)
                        .expect("failed to insert cf_clearance cookie");
                }

                client
            })
            .await
            .clone()
    }

    async fn get_random_common(client: Client) {
        let res = client.get_random().await.unwrap();
        for url in res.iter_image_urls() {
            dbg!(url);
        }

        let english_title = res.title.english;
        assert!(!english_title.is_empty());
    }

    #[tokio::test]
    async fn get_random() {
        get_random_common(get_client().await).await;
    }

    async fn get_index_common(client: Client) {
        let page = NonZeroU64::new(1).unwrap();
        let index = client.get_index(page).await.unwrap();
        assert!(index.num_pages != 0);
        assert!(index.per_page != 0);
    }

    #[tokio::test]
    async fn get_index() {
        get_index_common(get_client().await).await;
    }
}
